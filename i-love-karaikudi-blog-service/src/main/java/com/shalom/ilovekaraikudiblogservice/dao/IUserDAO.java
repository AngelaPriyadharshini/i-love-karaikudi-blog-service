package com.shalom.ilovekaraikudiblogservice.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shalom.ilovekaraikudiblogservice.dto.UserDTO;
import com.shalom.ilovekaraikudiblogservice.entity.User;

@Repository
public interface IUserDAO extends JpaRepository<User, Integer> {
	public List<User> findAll();
	
	public User findByUsername(String username);

	public User save(User user);
	
//	public User loginUser(UserDTO userDTO);

	public User findByUsernameAndPassword(String username, String password);
}
