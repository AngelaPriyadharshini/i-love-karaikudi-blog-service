package com.shalom.ilovekaraikudiblogservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shalom.ilovekaraikudiblogservice.entity.User;
import com.shalom.ilovekaraikudiblogservice.entity.VerificationToken;

public interface ITokenDAO extends JpaRepository<VerificationToken, Integer> {
	public VerificationToken findByToken(String token);

	public VerificationToken findByUser(User user);

	public VerificationToken save(VerificationToken token);

}
