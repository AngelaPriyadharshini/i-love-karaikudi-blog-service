package com.shalom.ilovekaraikudiblogservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shalom.ilovekaraikudiblogservice.entity.Authority;

@Repository
public interface IRoleDAO extends JpaRepository<Authority, Integer> {

	public Authority findByAuthority(String role);

}
