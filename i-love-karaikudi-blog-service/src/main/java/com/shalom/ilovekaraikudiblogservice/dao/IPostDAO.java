/**
 * 
 */
package com.shalom.ilovekaraikudiblogservice.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shalom.ilovekaraikudiblogservice.entity.Post;

@Repository
public interface IPostDAO extends JpaRepository<Post, Integer> {

	public List<Post> findAll();

	public Post findById(int id);

	// public Post save(Post post);
}
