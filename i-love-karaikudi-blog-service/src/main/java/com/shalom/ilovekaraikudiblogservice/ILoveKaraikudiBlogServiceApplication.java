package com.shalom.ilovekaraikudiblogservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = {"com.shalom.ilovekaraikudiblogservice"})
@EnableAutoConfiguration
public class ILoveKaraikudiBlogServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ILoveKaraikudiBlogServiceApplication.class, args);
	}
}