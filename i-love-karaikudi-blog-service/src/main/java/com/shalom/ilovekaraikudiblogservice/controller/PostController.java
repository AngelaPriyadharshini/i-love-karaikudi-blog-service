package com.shalom.ilovekaraikudiblogservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shalom.ilovekaraikudiblogservice.entity.Post;
import com.shalom.ilovekaraikudiblogservice.service.IPostService;

@RestController
@RequestMapping("/api")
public class PostController {

	@Autowired
	private IPostService postService;

	@GetMapping("/posts")
	public List<Post> getAllPosts() {
		return postService.getPosts();
	}

	@GetMapping("/posts/{id}")
	public Post getPostById(@PathVariable("id") int id) {
		return postService.getSinglePost(id);
	}

	public Post createPost() {
		return null;
	}

	@PostMapping("/posts")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Post approvePost(@RequestBody Post post ) {
		return postService.save(post);
	}

}
