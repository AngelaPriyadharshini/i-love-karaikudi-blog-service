package com.shalom.ilovekaraikudiblogservice.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.shalom.ilovekaraikudiblogservice.dto.UserDTO;
import com.shalom.ilovekaraikudiblogservice.entity.User;
import com.shalom.ilovekaraikudiblogservice.entity.VerificationToken;
import com.shalom.ilovekaraikudiblogservice.event.OnRegistrationSuccessEvent;
import com.shalom.ilovekaraikudiblogservice.service.IUserService;

@RestController
public class AccountController {
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	private MessageSource messages;
	
	private Logger logger = Logger.getLogger(getClass().getName());

@GetMapping("/hello")
public List<User> hello() {
	List<User> getUsers = new ArrayList<User>();
	getUsers = userService.findAll();
	return getUsers;
}

@GetMapping("/registration")
public String showRegistrationForm(Model model) {
	UserDTO userDto = new UserDTO();
	model.addAttribute("user", userDto);
	return "registration";
}

@PostMapping("/registration")
public String registerNewUser(@ModelAttribute("user") UserDTO userDto, BindingResult result, WebRequest request, Model model) {
	User registeredUser = new User();
	String userName = userDto.getUserName();
	if (result.hasErrors()) {
		return "registration";
	}
	registeredUser = userService.findByUsername(userName);
	if(registeredUser!=null) {
		model.addAttribute("error","There is already an account with this username: " + userName);
		logger.info("There is already an account with this username: " + userName);
		return "registration";
	}

	registeredUser = userService.registerUser(userDto);
	try {
		String appUrl = request.getContextPath();
		eventPublisher.publishEvent(new OnRegistrationSuccessEvent(registeredUser, request.getLocale(),appUrl));
	}catch(Exception re) {
		re.printStackTrace();
//		throw new Exception("Error while sending confirmation email");
	}
	return "registrationSuccess";
}

@GetMapping("/confirmRegistration")
public String confirmRegistration(WebRequest request, Model model,@RequestParam("token") String token) {
	Locale locale=request.getLocale();
	VerificationToken verificationToken = userService.getVerificationToken(token);
	if(verificationToken == null) {
		String message = messages.getMessage("auth.message.invalidToken", null, locale);
		model.addAttribute("message", message);
		return "redirect:access-denied";
	}
	User user = verificationToken.getUser();
	Calendar calendar = Calendar.getInstance();
	if((verificationToken.getExpiryDate().getTime()-calendar.getTime().getTime())<=0) {
		String message = messages.getMessage("auth.message.expired", null, locale);
		model.addAttribute("message", message);
		return "redirect:access-denied";
	}
	
	user.setEnabled(true);
	userService.enableRegisteredUser(user);
	return null;
}
}
