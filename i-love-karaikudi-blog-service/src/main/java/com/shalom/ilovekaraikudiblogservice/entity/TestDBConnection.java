package com.shalom.ilovekaraikudiblogservice.entity;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestDBConnection {

	public static void main(String[] args) {
		String jdbcUrl = "jdbc:mysql://localhost:3306/ILoveKaraikudi?useSSL=false";
		String user = "root";
		String pass = "mysqlP@ssw0rd";

		try {
			System.out.println("Connecting to database: " + jdbcUrl);
			Connection myConn = DriverManager.getConnection(jdbcUrl, user, pass);
			System.out.println("Connection successfull!!!" + myConn);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

}