package com.shalom.ilovekaraikudiblogservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "authorities")
public class Authority {

	@Id
	@Column(name = "authority")
	private String authority;

	public String getAuthority() {
		return authority;
	}

	public void setRolename(String authority) {
		this.authority = authority;
	}

	@Override
	public String toString() {
		return "Authority [ rolename=" + authority + "]";
	}

}
