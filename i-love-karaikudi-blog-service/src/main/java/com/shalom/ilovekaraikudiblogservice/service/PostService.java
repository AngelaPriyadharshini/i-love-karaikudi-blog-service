/**
 * 
 */
package com.shalom.ilovekaraikudiblogservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shalom.ilovekaraikudiblogservice.dao.IPostDAO;
import com.shalom.ilovekaraikudiblogservice.entity.Post;

@Service
public class PostService implements IPostService {

	@Autowired
	private IPostDAO postDAO;

	@Override
	public List<Post> getPosts() {
		return postDAO.findAll();
	}

	@Override
	public Post getSinglePost(int id) {
		return postDAO.findById(id);
	}

	@Override
	public Post save(Post post) {
		return postDAO.save(post);
	}

}
