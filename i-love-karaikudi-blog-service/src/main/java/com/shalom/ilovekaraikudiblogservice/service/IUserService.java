package com.shalom.ilovekaraikudiblogservice.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.shalom.ilovekaraikudiblogservice.dto.UserDTO;
import com.shalom.ilovekaraikudiblogservice.entity.User;
import com.shalom.ilovekaraikudiblogservice.entity.VerificationToken;

@Service
public interface IUserService extends UserDetailsService {
	
	public User registerUser(UserDTO userDto);

	public List<User> findAll();

	public User findByUsername(String username);

	// public User loginUser(UserDTO userDTO);

	public User findByUsernameAndPassword(String username, String password);

	public void createVerificationToken(User user, String token);

	public VerificationToken getVerificationToken(String verificationToken);

	public void enableRegisteredUser(User user);

}
