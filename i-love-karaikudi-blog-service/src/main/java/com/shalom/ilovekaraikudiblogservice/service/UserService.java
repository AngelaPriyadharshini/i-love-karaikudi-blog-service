package com.shalom.ilovekaraikudiblogservice.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.shalom.ilovekaraikudiblogservice.dao.IRoleDAO;
import com.shalom.ilovekaraikudiblogservice.dao.ITokenDAO;
import com.shalom.ilovekaraikudiblogservice.dao.IUserDAO;
import com.shalom.ilovekaraikudiblogservice.dto.UserDTO;
import com.shalom.ilovekaraikudiblogservice.entity.Authority;
import com.shalom.ilovekaraikudiblogservice.entity.User;
import com.shalom.ilovekaraikudiblogservice.entity.VerificationToken;
//import com.shalom.ilovekaraikudiblogservice.entity.VerificationToken;

@Service
public class UserService implements IUserService {
	@Autowired
	private IUserDAO userDAO;
	
	@Autowired
	private IRoleDAO roleDAO;

	@Autowired
	private ITokenDAO tokenDAO;

	@Autowired
	private PasswordEncoder passwordEncoder;

	// logger
	private Logger logger = Logger.getLogger(getClass().getName());

	@Override
	@Transactional
	public User registerUser(UserDTO userDto) {

		User user = new User();
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setUsername(userDto.getUserName());
		String hashedPassword = passwordEncoder.encode(userDto.getPassword());
		user.setPassword(hashedPassword);
		user.setEnabled(userDto.isEnabled());
		user.setEmail(userDto.getEmail());

		user.setRoles(Arrays.asList(roleDAO.findByAuthority("ROLE_USER")));
		userDAO.save(user);
		return user;
	}

	@Override
	@Transactional
	public User findByUsername(String username) {
		return userDAO.findByUsername(username);
	}

	@Override
	public User findByUsernameAndPassword(String username, String password) {

		return userDAO.findByUsernameAndPassword(username, password);
	}

//	@Transactional
//	@Override
//	public User loginUser(UserDTO userDTO) {
//		return userDAO.loginUser(userDTO);
//	}

	@Transactional
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userDAO.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		try {
			if (user.isEnabled() != true) {
				throw new UsernameNotFoundException("Please enable your account.");
			}
		} catch (UsernameNotFoundException e) {
			e.printStackTrace();
		}

		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				user.isEnabled(), true, true, true, mapRolesToAuthorities(user.getAuthorities()));
	}

	private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Authority> authorities) {
		return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority.getAuthority())).collect(Collectors.toList());
	}

	@Override
	public void createVerificationToken(User user, String token) {
		VerificationToken newUserToken = new VerificationToken(token, user);
		tokenDAO.save(newUserToken);
	}

	@Override
	@Transactional
	public VerificationToken getVerificationToken(String verificationToken) {
		return tokenDAO.findByToken(verificationToken);
	}

	@Override
	@Transactional
	public void enableRegisteredUser(User user) {
		userDAO.save(user);
	}

	@Override
	public List<User> findAll() {
		return userDAO.findAll();
	}

}
