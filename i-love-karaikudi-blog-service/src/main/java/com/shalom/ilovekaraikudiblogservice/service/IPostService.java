package com.shalom.ilovekaraikudiblogservice.service;

import java.util.List;

import com.shalom.ilovekaraikudiblogservice.entity.Post;

public interface IPostService {
	public List<Post> getPosts();
	
	public Post getSinglePost(int id);
	
	public Post save(Post post);
}
