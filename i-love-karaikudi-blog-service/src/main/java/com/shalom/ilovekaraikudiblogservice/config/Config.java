package com.shalom.ilovekaraikudiblogservice.config;

import java.beans.PropertyVetoException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableWebSecurity
@EnableTransactionManagement
@ComponentScan("com.shalom.ilovekaraikudiblogservice")
@PropertySource("classpath:application.properties")
public class Config {

	// properties variable
	@Autowired
	private Environment env;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	// logger
	private Logger logger = Logger.getLogger(getClass().getName());

	String[] packagesToScan = new String[] { "com.shalom.ilovekaraikudiblogservice.entity" };

	// bean for security datasource

	@Bean
	public DataSource securityDS() {
		// connection pool
		ComboPooledDataSource securityDS = new ComboPooledDataSource();

		// jdbc driver classs
		try {
			securityDS.setDriverClass(env.getProperty("spring.datasource.driver-class-names"));
		} catch (PropertyVetoException ex) {
			throw new RuntimeException(ex);
		}
		// log connection properties
		logger.info(">>> jdbc.url=" + env.getProperty("spring.datasource.url"));
		logger.info(">>> jdbc.user=" + env.getProperty("spring.datasource.username"));

		// set db connection properties (optional)
		securityDS.setJdbcUrl(env.getProperty("spring.datasource.url"));
		securityDS.setUser(env.getProperty("spring.datasource.username"));
		securityDS.setPassword(env.getProperty("spring.datasource.password"));

		// connection pool properties
		securityDS.setInitialPoolSize(getIntProperty("spring.datasource.tomcat.initial-size"));
		securityDS.setMinPoolSize(getIntProperty("spring.datasource.tomcat.min-idle"));
		securityDS.setMaxPoolSize(getIntProperty("spring.datasource.tomcat.max-idle"));
		securityDS.setMaxIdleTime(getIntProperty("spring.datasource.tomcat.max-wait"));

		return securityDS;
	}

	private int getIntProperty(String propertyName) {
		String property = env.getProperty(propertyName);

		// to int
		int intproperty = Integer.parseInt(property);

		return intproperty;
	}

	private Properties getHibernateProperties() {

		// set hibernate properties
		Properties props = new Properties();

		props.setProperty("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
		props.setProperty("hibernate.show_sql", env.getProperty("spring.jpa.show-sql"));

		return props;
	}

	// @Bean
	// public LocalSessionFactoryBean sessionFactory() {
	//
	// // create session factory
	// LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	//
	// // set the properties
	// sessionFactory.setDataSource(securityDS());
	// sessionFactory.setPackagesToScan(packagesToScan);
	// sessionFactory.setHibernateProperties(getHibernateProperties());
	//
	// return sessionFactory;
	// }
//	@Bean
//	public SessionFactory getSessionFactory(EntityManagerFactory emf) {
//		if (emf.unwrap(SessionFactory.class) == null) {
//			throw new NullPointerException("factory is not a hibernate factory");
//		}
//		return emf.unwrap(SessionFactory.class);
//	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
		entityManager.setDataSource(securityDS());
		entityManager.setPackagesToScan(packagesToScan);
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		entityManager.setJpaVendorAdapter(vendorAdapter);
		entityManager.setJpaProperties(getHibernateProperties());
		return entityManager;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);

		return transactionManager;
	}
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
	return new PersistenceExceptionTranslationPostProcessor();
	 }
	// @Bean
	// @Autowired
	// public HibernateTransactionManager transactionManager(SessionFactory
	// sessionFactory) {
	//
	// // setup transaction manager based on session factory
	// HibernateTransactionManager txManager = new HibernateTransactionManager();
	// txManager.setSessionFactory(sessionFactory);
	//
	// return txManager;
	// }
	
	@Bean(name = "mailSender")
	public JavaMailSender javaMailService() {
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		javaMailSender.setHost("smtp.gmail.com");
		javaMailSender.setPort(587);
		javaMailSender.setProtocol("smtp");
		javaMailSender.setUsername("angela.webdevtesting@gmail.com");
		javaMailSender.setPassword("WebDevTesting@321");
		Properties mailProperties = new Properties();
		mailProperties.put("mail.smtp.auth", "true");
		mailProperties.put("mail.smtp.starttls.enable", "true");
		mailProperties.put("mail.smtp.debug", "true");
		javaMailSender.setJavaMailProperties(mailProperties);
		return javaMailSender;
	}
	
	@Bean
     public MessageSource messageSource() {
     final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
     messageSource.setBasename("classpath:messages");
     messageSource.setUseCodeAsDefaultMessage(true);
     messageSource.setDefaultEncoding("UTF-8");
     messageSource.setCacheSeconds(0);
     return messageSource;
     }
}
